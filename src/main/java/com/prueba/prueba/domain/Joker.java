package com.prueba.prueba.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Data
public class Joker {

    private String icon_url;
    private String id;
    private String url;
    private String value;
    private String created_at;
    private String updated_at;
    private ArrayList<String> categories;
}
