package com.prueba.prueba.domain;


import lombok.Data;

@Data
public class JokerResponse {

    private String icon_url;
    private String id;
    private String url;
    private String value;

}
