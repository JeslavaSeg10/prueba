package com.prueba.prueba.mapper;

import com.prueba.prueba.domain.Joker;
import com.prueba.prueba.domain.JokerResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface JokerMapper {

    JokerResponse toResponseDto(Joker joker);

}
