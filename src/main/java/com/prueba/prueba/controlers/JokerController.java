package com.prueba.prueba.controlers;

import com.prueba.prueba.domain.JokerResponse;
import com.prueba.prueba.servicies.JokerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/joke")
public class JokerController {

    private  final JokerServices jokeServices;


    @GetMapping()
    public ResponseEntity<List<JokerResponse>>findJokes(){
        return ResponseEntity.ok(jokeServices.findJoke());
    }

}
