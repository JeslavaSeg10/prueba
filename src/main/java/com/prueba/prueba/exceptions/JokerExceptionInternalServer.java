package com.prueba.prueba.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class JokerExceptionInternalServer extends RuntimeException{
    public JokerExceptionInternalServer(String message) {
        super(message);
    }
}
