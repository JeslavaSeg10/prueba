package com.prueba.prueba.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class JokerExceptionNotFount extends RuntimeException{
    public JokerExceptionNotFount(String message) {
        super(message);
    }
}
