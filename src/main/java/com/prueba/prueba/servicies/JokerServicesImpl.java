package com.prueba.prueba.servicies;

import com.prueba.prueba.constants.MessageResource;
import com.prueba.prueba.domain.Joker;
import com.prueba.prueba.domain.JokerResponse;
import com.prueba.prueba.exceptions.JokerExceptionInternalServer;
import com.prueba.prueba.exceptions.JokerExceptionNotFount;
import com.prueba.prueba.mapper.JokerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class JokerServicesImpl implements JokerServices {

    private final RestTemplate restTemplate;

    private  final JokerMapper jokerMapper;

    @Value("${URL_JOKES}")
    private String URL_JOKES;

    @Override
    public List<JokerResponse> findJoke() {
        List<JokerResponse> responses = new ArrayList<>();
        Integer size =0;
        while (size < 25) {
            Joker jokerResponseTemplate = getJokerResponseTemplate();
            validateID(responses, jokerResponseTemplate);
            size = responses.size();
        }
        return responses;
    }

    private void validateID(List<JokerResponse> responses, Joker jokerResponseTemplate) {
        Optional<JokerResponse> exist = responses.stream().filter(jokerResponse -> jokerResponse.getId() == jokerResponseTemplate.getId()).findFirst();
        if(!exist.isPresent()) {
            responses.add(jokerMapper.toResponseDto(jokerResponseTemplate));
        }
    }

    private Joker getJokerResponseTemplate() {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            HttpEntity<Joker> entity = new HttpEntity<>(headers);
            ResponseEntity<Joker> jokerResponseEntity = restTemplate.exchange(URL_JOKES, HttpMethod.GET, entity, Joker.class);
            Joker joker = jokerResponseEntity.getBody();
            return joker;
        }catch( HttpClientErrorException ex){
            if(ex.getStatusCode().is4xxClientError()){
                throw new JokerExceptionNotFount(MessageResource.NOT_FOUND);
            }

            if(ex.getStatusCode().is5xxServerError()){
                throw new JokerExceptionInternalServer(MessageResource.INTERNAL_SERVER_ERROR);
            }
        }
        return null;
    }

}
