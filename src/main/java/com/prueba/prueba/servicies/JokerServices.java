package com.prueba.prueba.servicies;

import com.prueba.prueba.domain.JokerResponse;

import java.util.List;

public interface JokerServices {

    List<JokerResponse> findJoke();

}
